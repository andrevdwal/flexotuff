#!/bin/bash

echo starting deployment

# vars
BASE=`dirname ${0}`
TAB="  "
SRCDIR=$BASE
DEPDIR="${BASE}/www"

echo - cleaning
rm -rf $DEPDIR
mkdir $DEPDIR

echo - deploy directories
declare -a arrcopydirs=(
  "app"
  "config"
)

for dir in "${arrcopydirs[@]}"
do
  echo "${TAB} - ${dir}/"
  mkdir -p `dirname ${DEPDIR}/${dir}`
  cp -R $SRCDIR/$dir/ $DEPDIR/$dir/
done

echo - deploy files
declare -a arrcopyfile=(
  ".htaccess"
  "index.php"
  "vendor/autoload.php"
  "vendor/composer/*.php"

  "vendor/bcosca/fatfree-core/base.php"
  "vendor/bcosca/fatfree-core/db/cursor.php"
  "vendor/bcosca/fatfree-core/db/sql.php"
  "vendor/bcosca/fatfree-core/db/sql/mapper.php"
  "vendor/bcosca/fatfree-core/magic.php"
  "vendor/bcosca/fatfree-core/template.php"

  "public/app/app.js"

  "public/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css"
  "public/adminlte/bower_components/bootstrap/dist/fonts/glyphicons-halflings-regular.woff2"
  "public/adminlte/bower_components/bootstrap/dist/fonts/glyphicons-halflings-regular.woff"
  "public/adminlte/bower_components/bootstrap/dist/fonts/glyphicons-halflings-regular.ttf"
  "public/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"
  "public/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"
  "public/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css"
  "public/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"
  "public/adminlte/bower_components/font-awesome/css/font-awesome.min.css"
  "public/adminlte/bower_components/font-awesome/fonts/fontawesome-webfont.woff"
  "public/adminlte/bower_components/font-awesome/fonts/fontawesome-webfont.woff2"
  "public/adminlte/bower_components/Ionicons/css/ionicons.min.css"
  "public/adminlte/bower_components/jquery/dist/jquery.min.js"
  "public/adminlte/dist/css/AdminLTE.min.css"
  "public/adminlte/dist/css/skins/_all-skins.min.css"
  "public/adminlte/dist/js/adminlte.min.js"

  "public/bootbox/bootbox.min.js"
)

for fil in "${arrcopyfile[@]}"
do
  echo "${TAB} - ${fil}"
  mkdir -p `dirname ${DEPDIR}/${fil}`
  cp $SRCDIR/$fil `dirname ${DEPDIR}/${fil}/`
done

echo - removing .DS_Store files
find $DEPDIR -name '.DS_Store' -type f -delete

echo done
