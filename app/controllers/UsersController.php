<?php
class UsersController extends SecureController {

  public function listUsers() {

    $user = new User($this->db);
    $this->f3->set('users', $user->all());

    $this->f3->set('pageID', 'users-list');
    $this->f3->set('pageTitle', 'Users');
    $this->f3->set('pageDescription', 'All your little minions...');

    $this->f3->set('view', 'users/userlist.html');
  }

  public function newUser() {

    $this->f3->set('pageID', 'users-new');
    $this->f3->set('pageTitle', 'New User');
    $this->f3->set('view', 'users/useredit.html');
  }

  public function editUser() {

    $params = $this->f3->get('PARAMS');

    $user = new User($this->db);
    $user->getByID($params['userID']);
    $this->f3->set('user', $user);

    $this->f3->set('pageID', 'users-edit');
    $this->f3->set('pageTitle', 'Edit User');

    $this->f3->set('view', 'users/useredit.html');
  }

  public function deleteUser() {

    $params = $this->f3->get('PARAMS');

    $user = new User($this->db);
    $user->delete($params['userID']);

    // output
    $this->addAlert(AlertMessage::success("Success!", "User deleted."));
    $this->listUsers();
  }

  public function updateUser() {

    $params = $this->f3->get('POST');

    $isNew = $params['ID'] == 0;
    $userNameEmpty = $params['UserName'] == '';
    $displayNameEmpty = $params['DisplayName'] == '';
    $passwordEmpty = $params['Password'] == '';
    $passwordMatch = $params['Password'] == $params['Password2'];

    // validate
    if($isNew && ($passwordEmpty || $userNameEmpty || $displayNameEmpty)) {
      $this->addAlert(AlertMessage::danger("Missing fields:", "Please capture all the fields and try again."));
      $this->newUser();
      return;
    }

    if($isNew && !$passwordEmpty && !$passwordMatch) {
      $this->addAlert(AlertMessage::danger("Data mismatch:", "Passwords do not match."));
      $this->newUser();
      return;
    }

    if(!$isNew && ($userNameEmpty || $displayNameEmpty)) {
      $this->addAlert(AlertMessage::danger("Missing fields:", "Please capture all the fields and try again."));
      $this->f3->set('PARAMS.userID', $params['ID']);
      $this->editUser();
      return;
    }

    if(!$isNew && !$passwordEmpty && !$passwordMatch) {
      $this->addAlert(AlertMessage::danger("Data mismatch:", "Passwords do not match."));
      $this->f3->set('PARAMS.userID', $params['ID']);
      $this->editUser();
      return;
    }

    // update

    if($isNew) {
      $user = new User($this->db);
      $user->copyFrom('POST');
      $user->Password = md5($user->Password);
      $user->save();

    } else {
      $user = new User($this->db);
      $user->load(array('id=?', $params['ID']));
      $user->copyFrom('POST');

      if($passwordEmpty) {
        $existingUser = new User($this->db);
        $existingUser->getByID($params['ID']);
        $user->Password = $existingUser->Password;
      } else {
        $user->Password = md5($user->Password);
      }
      $user->update();
    }

    // output
    $this->addAlert(AlertMessage::success("Success!", "User updated."));
    $this->listUsers();
  }
}
?>
