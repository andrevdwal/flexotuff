<?php
class ClientsController extends SecureController {

  public function listClients() {

    $client = new Client($this->db);
    $this->f3->set('clients', $client->all());

    $this->f3->set('pageID', 'clients-list');
    $this->f3->set('pageTitle', 'Clients');
    $this->f3->set('pageDescription', '');

    $this->f3->set('view', 'clients/clientlist.html');
  }

  public function newClient() {

    $this->f3->set('pageID', 'clients-new');
    $this->f3->set('pageTitle', 'New Client');
    $this->f3->set('view', 'clients/clientedit.html');
  }

  public function editClient() {

    $params = $this->f3->get('PARAMS');

    $client = new Client($this->db);
    $client->getByID($params['clientID']);
    $this->f3->set('client', $client);

    $this->f3->set('pageID', 'client-edit');
    $this->f3->set('pageTitle', 'Edit Client');

    $this->f3->set('view', 'clients/clientedit.html');
  }

  public function deleteClient() {

    $params = $this->f3->get('PARAMS');

    $client = new Client($this->db);
    $client->delete($params['clientID']);

    // output
    $this->addAlert(AlertMessage::success("Success!", "Client deleted."));
    $this->listClients();
  }

  public function updateClient() {

    $params = $this->f3->get('POST');

    $isNew = $params['ID'] == 0;
    $nameEmpty = $params['Name'] == '';

    // validate
    if($nameEmpty) {
      $this->addAlert(AlertMessage::danger("Missing fields:", "Please capture all the fields and try again."));

      if($isNew) {
        $this->newClient();
      }
      else {
        $this->f3->set('PARAMS.clientID', $params['ID']);
        $this->editClient();
      }
      return;
    }

    // udpate
    if($isNew) {
      $client = new Client($this->db);
      $client->copyFrom('POST');
      $client->save();

    } else {
      $client = new Client($this->db);
      $client->load(array('id=?', $params['ID']));
      $client->copyFrom('POST');
      $client->update();
    }

    // output
    $this->addAlert(AlertMessage::success("Success!", "Client updated."));
    $this->listClients();
  }
}
?>
