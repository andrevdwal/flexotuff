<?php
class SecureController extends Controller {

  private $alerts;

  function __construct() {
    $this->alerts = array();
    parent::__construct();
  }

  function afterroute() {

    $this->f3->set('alerts', $this->alerts);
    echo Template::instance()->render('layout.html');
  }

  protected function addAlert($alert) {
    array_push($this->alerts, $alert);
  }
}
?>
