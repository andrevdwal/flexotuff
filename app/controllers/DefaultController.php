<?php
class DefaultController extends SecureController {

  public function index() {
    $this->f3->set('pageTitle', 'Default');
    $this->f3->set('pageDescription', 'All is lost');
    $this->f3->set('view', '404.html');
  }

}
?>
