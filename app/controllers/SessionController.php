<?php
class SessionController extends Controller {

  public function index() {
    echo Template::instance()->render('login.html');
    $this->f3->clear('SESSION.flash');
  }

  public function login() {
    $this->f3->reroute('../users/list');
  }
}
?>
