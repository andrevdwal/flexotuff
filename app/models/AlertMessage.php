 <?php
 class AlertMessage {

   public $type;
   public $label;
   public $text;

   private function __construct($type, $label, $text) {
     $this->type = $type;
     $this->label = $label;
     $this->text = $text;
   }

   public static function success($label, $text) {
     return new AlertMessage("success", $label, $text);
   }

   public static function info($label, $text) {
     return new AlertMessage("info", $label, $text);
   }

   public static function warning($label, $text) {
     return new AlertMessage("warning", $label, $text);
   }

   public static function danger($label, $text) {
     return new AlertMessage("danger", $label, $text);
   }
 }
?>
