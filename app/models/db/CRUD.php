<?php
class CRUD extends \DB\SQL\Mapper {

  public function __construct(DB\SQL $db, $tableName) {
    parent::__construct($db, $tableName);
  }

  public function all() {
    $this->load();
    return $this->query;
  }

  public function add() {
    $this->copyFrom('POST');
    $this->save();
  }

  public function getByID($id) {
    $this->load(array('id=?', $id));
    $this->copyTo('POST');
  }

  public function udpate($id) {
    $this->load(array('id=?',$id));
    $this->copyFrom('POST');
    $this->update();
  }

  public function delete($id) {
    $this->load(array('id=?',$id));
    $this->erase();
  }
}
?>
